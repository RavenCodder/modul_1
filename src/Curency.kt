
    abstract class Currency {

       abstract var quantity: Double
        abstract val name: String


        enum class TypeOfValute(var valute: String) {
            Dollar("$"),
            Euro("eur"),
            Yena("yen")
            //....
        }




    // ----------------------------- Functions -----------------------------------------------------------------

        fun toDollar(a: Currency) {
            var result: Double? = null
            when (a.name) {
                Currency.TypeOfValute.Euro.valute -> result = a.quantity * TypeOfExchange.EuroToDollar.pair
                Currency.TypeOfValute.Yena.valute -> result = a.quantity * TypeOfExchange.YenaToDollar.pair
                Currency.TypeOfValute.Dollar.valute -> println("Cant convert to itself")
                else -> println("Unknown currency")
            }
            if (result != null) {
                val formatedResult = String.format("%.2f", result)
                return println("${a.quantity}${a.name} = ${TypeOfValute.Dollar.valute}$formatedResult")
            }
        }

        fun toEuro(a: Currency) {
            var result: Double? = null
            when (a.name) {
                Currency.TypeOfValute.Euro.valute -> println("Cant convert to itself")
                Currency.TypeOfValute.Dollar.valute -> result = a.quantity * TypeOfExchange.DollarToEuro.pair
                else -> println("Unknown currency")
            }
            if (result != null) {
                val formatedResult = String.format("%.2f", result)
                return println("${a.name}${a.quantity} = $formatedResult${TypeOfValute.Euro.valute}")
            }
        }


        fun plus(a: Currency, b: Currency) {
            var result: Double? = null
            if(a::class != b::class) {
                println("You should have same currencies")
            }
            if(a::class == b::class) {
                result = a.quantity + b.quantity
            }
            if(result != null) {
                when(a.name) {
                 TypeOfValute.Dollar.valute -> println("${a.name}${a.quantity} + ${b.name}${b.quantity} = $name$result")
                 else -> println("${a.quantity}${a.name} + ${b.quantity}${b.name} = $result$name")
                }
            }
        }


        fun minus(a: Currency, b: Currency) {
            var result: Double? = null
            if(a::class != b::class) {
                println("You should have same currencies")
            }
            if(a::class == b::class) {
                result = a.quantity - b.quantity
            }
            if(result != null) {
                when(a.name) {
                    TypeOfValute.Dollar.valute -> println("${a.name}${a.quantity} - ${b.name}${b.quantity} = $name$result")
                    else -> println("${a.quantity}${a.name} - ${b.quantity}${b.name} = $result$name")
                }
            }
        }

    }










