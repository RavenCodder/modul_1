

enum class TypeOfExchange(var pair: Double) {
    DollarToEuro(0.8),
    EuroToDollar(1.5),
    YenaToDollar(0.08),
    DollarToYena(24.0)
    //.....
}